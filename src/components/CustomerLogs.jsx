import React, {useState, useEffect} from "react";
import Tabs from "./Tabs";
import LogsTable from "./LogsTable";
import Charts from "./Charts";
import NextSteps from './NextSteps';

const CustomerLogs = ({customerId, logsData}) => {
  const [activeTab, setActiveTab] = useState("");
  const onTabChangeHanlder = selectedTab => {
    // checks if tab is a valid option //
    if (tabs.some(({id}) => id === selectedTab)) {
      setActiveTab(selectedTab);
    }
  };

  const hasData = (activeTab && logsData.length > 0);

  useEffect(() => {
    setActiveTab("logs");
  }, [logsData.length]);

  const tabs = [
    {
      id: "logs",
      text: "Logs"
    },
    {
      id: "analytics",
      text: "Analytics"
    },
    {
      id: "nextSteps",
      text: "Next Steps"
    }
  ];

  return (
    <>
      {hasData && (
        <div style={{marginTop: 30}}>
          <Tabs
            tabs={tabs}
            activeTab={activeTab}
            onTabChangeHanlder={onTabChangeHanlder}
          />
          <div>
            {activeTab === "logs" && (
              <LogsTable customerId={customerId} data={logsData} />
            )}
            {activeTab === 'analytics' && (
              <Charts />
            )}
            {activeTab === "nextSteps" && (
              <NextSteps />
            )}
          </div>
        </div>
      )}
    </>
  );
};

export default CustomerLogs;
