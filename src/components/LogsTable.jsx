import React from "react";
import MaterialTable from "material-table";

const actionTypesMatch = [
  {
    color: '#01D5DF',
    type: 'DEPOSIT'
  },
  {
    color: '#7079EC',
    type: 'BALANCE_CHANGED'
  },
  {
    color: '#A14EF3',
    type: 'KAMBI_BET'
  },
  {
    color: '#4B9AE7',
    type: 'RACE_BET'
  },
  {
    color: '#A54CF3',
    type: 'REGISTRATION'
  },
  {
    color: '#01D5DF',
    type: 'BONUS_CLAIMED'
  }
];

const LogsTable = ({customerId, data}) => {
  return (
    <MaterialTable
      title="Basic Search Preview"
      columns={[
        {
          title: "Id",
          field: "id"
        },
        {
          title: "Action Type",
          field: "actionType",
          render: rowSpan => {
            const matchedActionType = actionTypesMatch.find(({type}) => type === rowSpan.actionType);
            const style = matchedActionType ? { background: matchedActionType.color, color: '#fff', padding: '.4rem .7rem', borderRadius: 25 } : {};

            return (
              <span style={style}>
                {rowSpan.actionType}
              </span>
            )
          }
        },
        {title: "Amount", field: "amount"},
        {title: "Currency", field: "currency"},
        {title: "Session Id", field: "sessionId"},
        {title: "Channel", field: "channel"},
        {title: "Date", field: "date"}
      ]}
      data={data}
      options={{
        search: true,
        showTitle: false,
        padding: "dense",
        pageSize: 10
      }}
    />
  );
};

export default LogsTable;
