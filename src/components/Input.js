import React from "react";
import TextField from "@material-ui/core/TextField";
import "./Input.css";

function Input({onChange, value}) {
  return (
    <TextField
      id="customerId"
      type="number"
      label="Customer ID"
      onChange={onChange}
      value={value}
    />
  );
}

export default Input;
