import React from "react";
import { Chart } from "react-google-charts";

const colors = [
  "#01D5DF",
  "#1FBEE2",
  "#5095E8",
  "#7079EC",
  "#8F5DF0",
  "#A14EF3",
  "#B73BF6",
  "#CA2CF8",
  "#4B9AE7",
  "#A54CF3",
  "#459EE6",
  "#EE0BFD",
  "#01D5DF",
  "#B73BF6",
  "#5095E8",
];

function Charts() {
  const data = [
    { product: "Unibet", percentage: 20 },
    { product: "Maria", percentage: 30 },
    { product: "Igame", percentage: 40 },
    { product: "Otto", percentage: 10 },
  ];

  const chartData = [["Product", "Percentage"]];

  data.forEach(({ product, percentage }) => {
    chartData.push([product, percentage]);
  });

  return (
    <div>
      <Chart
        width={"500px"}
        height={"500px"}
        chartType="PieChart"
        loader={<div>Loading Chart</div>}
        data={chartData}
        options={{
          title: "My Daily Activities",
          // Just add this option
          is3D: false,
          titleTextStyle: {
            fontName: "Poppins", // i.e. 'Times New Roman'
            fontSize: 30, // 12, 18 whatever you want (don't specify px)
            bold: false, // true or false
            italic: false, // true of false
          },
          colors: colors,
        }}
      />
    </div>
  );
}

export default Charts;
