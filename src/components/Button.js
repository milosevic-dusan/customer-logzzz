import React from "react";
import "./Button.css";

function Button({inactive, text, ...rest}) {
  const className = inactive
    ? "contact100-form-btn btn-inactive"
    : "contact100-form-btn";

  return (
    <button {...rest} className={className} disabled={inactive}>
      <span>{text}</span>
    </button>
  );
}

export default Button;
