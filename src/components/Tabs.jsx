import React from "react";

const Tabs = ({tabs = [], activeTab, onTabChangeHanlder}) => {
  if (!tabs.length) return null;

  return (
    <div className="tabs-wrapper">
      {tabs.map(tab => (
        <button
          className={`tab ${tab.id === activeTab ? "isActive" : ""}`}
          key={tab.id}
          onClick={() => onTabChangeHanlder(tab.id)}
        >
          {tab.text}
        </button>
      ))}
    </div>
  );
};

export default Tabs;
