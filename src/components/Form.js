import React from "react";
import axios from "axios";
import Input from "./Input";
import Button from "./Button";
import './Form.css';

function Form({customerId, setCustomerId, setLogsData}) {
  const url = `http://localhost:9093/logbook?customerId=${customerId}`;
  const trackUrl = `http://localhost:9093/logbook/track?customerId=${customerId}`;

  const inputChange = e => {
    setCustomerId(e.target.value);
  };

  const handleSearch = e => {
    e.preventDefault();

    return axios
      .post(trackUrl)
      .then(response => {
        console.log(response);
        if (response.status === 200) {
          return axios.get(url).then(({data}) => {
            console.log(data);
            setLogsData(data.log);
          });
        }
      })
      .catch(function(error) {
        console.log(error);
      });
  };

  return (
    <>
      <Input onChange={inputChange} value={customerId} />
      <Button text="Search" inactive={!customerId} onClick={handleSearch} />
    </>
  );
}

export default Form;
