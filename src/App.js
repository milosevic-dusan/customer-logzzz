import React, {useState} from "react";
import "./App.css";
import Form from "./components/Form";
import CustomerLogs from "./components/CustomerLogs";

const App = () => {
  const [customerId, setCustomerId] = useState("");
  const [logsData, setLogsData] = useState([]);

  return (
    <div className="App">
      <div className="App-form">
        <span className="App-title">Customer logzz</span>
        <Form
          customerId={customerId}
          setCustomerId={setCustomerId}
          setLogsData={setLogsData}
        />
        <CustomerLogs customerId={customerId} logsData={logsData} />
      </div>
    </div>
  );
};

export default App;
